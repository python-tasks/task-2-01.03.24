class Vehicle:
    
    def __init__(self, made,model) -> None:
        self.made=made
        self.model=model
    def start_engine(self):
        print ("Engine started")   
    
    
class Car(Vehicle):
    def __init__(self,made, model) -> None:
        self.number_of_wheels=4
        super().__init__(made,model)
    def start_engine(self):
        print ("Car engine started")  
        super().start_engine()


car_1=Car(1963,"Chevrolet Corvette")
car_1.start_engine()       
vcl=Vehicle(1963,"Chevrolet Corvette")
vcl.start_engine()
 