class Vehicle:
    
    def __init__(self, made,model) -> None:
        self.made=made
        self.model=model
    def start_engine(self):
        print ("Engine started")   
    
    
class Car(Vehicle):
    def __init__(self,made, model) -> None:
        super().__init__(made,model)

    def start_engine(self):
        print ("Car engine started")  
        super().start_engine()

class ElectricVehicle:
    def __init__(self, battery_capacity):
        self.battery_capacity=battery_capacity
  
class ElectricCar(Car ,ElectricVehicle,):
    def __init__(self, made, model, battery_capacity):
        super().__init__(made, model)  
        ElectricVehicle.__init__(self, battery_capacity)
       
el_car = ElectricCar(2015,"Tesla Model X SUV", 100)
print(el_car.__dict__)
print(ElectricCar.__mro__) 
 