class Vehicle:
    vehicle_count = 0

    def __init__(self, made, model):
        self.make = made
        self.model = model
        self.increment_vehicle_count()

    def increment_vehicle_count(self):
        Vehicle.vehicle_count += 1

    def get_vehicle_count(self):
        return Vehicle.vehicle_count

class Car(Vehicle):
    def __init__(self, made, model):
        super().__init__(made, model)

car_1 = Car(1963,"Chevrolet Corvette")
vcl_1 = Vehicle(1960,"Shelby GT350")
vcl_2 = Vehicle(1970,"DeTomaso Pantera")
car_2 = Car(1960,"Jaguar E-Type")
vcl_3 = Vehicle(1992,"McLaren F1")

print("Vehicle count:", vcl_3.get_vehicle_count())  
