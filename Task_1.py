class Vehicle:
    
    def __init__(self, made,model) -> None:
        self.made=made
        self.model=model
    def __repr__(self):
        return f"Vehicle(made={self.made}, model='{self.model}')"    

class Car(Vehicle):
    def __init__(self,made, model) -> None:
        self.number_of_wheels=4
        super().__init__(made,model)

    def __repr__(self):
        return f"Car(made={self.made}, model='{self.model}', Number_of_wheels={self.number_of_wheels})" 

car_1=Car(1963,"Chevrolet Corvette")
print(repr(car_1))        
vcl=Vehicle(1963,"Chevrolet Corvette")
print(repr(vcl))
 